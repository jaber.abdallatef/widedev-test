<?php
// phpcs:disable
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert(
            [
            [
            'name' => "product_".rand(),
            'price' => 50
            ],
            [
                'name' => "product_".rand(),
                'price' => 60
            ],
            [
                'name' => "product_".rand(),
                'price' => 80
            ]
            ]
        );
    }
}
