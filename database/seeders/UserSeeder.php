<?php

/**
 * UserSeeder Class Doc Comment
 *
 * @category Class
 * @package  Database\Seeders
 * @author   Jaber <jaber.abdallatef@gmail.com>
 * @license  GNU General Public License
 * @link     ""
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


/**
 * UserSeeder Class Doc Comment
 *
 * @category Class
 * @package  Database\Seeders
 * @author   Jaber <jaber.abdallatef@gmail.com>
 * @license  GNU General Public License
 * @link     ""
 */


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
            [
            'name' => "user_".Str::random(2),
            'surname' => "user_".Str::random(2),
            'email' => "user_".Str::random(2).'@gmail.com',
            'password' => Hash::make('password'),
            ],
            [
                'name' => "user_".Str::random(2),
                'surname' => "user_".Str::random(2),
                'email' => "user_".Str::random(2).'@gmail.com',
                'password' => Hash::make('password'),
            ],
            [
                'name' => "user_".Str::random(2),
                'surname' => "user_".Str::random(2),
                'email' => "user_".Str::random(2).'@gmail.com',
                'password' => Hash::make('password'),
            ]
            ]
        );
    }
}
