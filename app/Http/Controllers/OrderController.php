<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Payment;
use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $user_id = $request->userid;
        if (isset($user_id) && $user_id != '') {
            $orders = Order::where('user_id', $user_id)->get();
        } else {
            $orders = Order::all();
        }
        return response()->json([
            'success' => true,
            'data' => $orders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'products' => 'required',
            'paymenttype' => 'required',
            'price' => 'required',
        ]);


        $data = $request->all();

        $orderData = [];
        $orderData['user_id'] = Auth::id();
        $orderData['price'] = $data['price'];
        $order = Order::create($orderData);

        $productData = $data['products'];
        $order->products()->attach($productData);

        $paymentData = [];
        $paymentData['type'] = $data['paymenttype'];
        $paymentData['price'] = $data['price'];
        $payment = new Payment($paymentData);
        $order->payment()->save($payment);

        return response()->json([
            'success' => true,
            'data' => $order
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $order = Order::where('id', $id)->with(['products', 'user', 'payment'])->get();

        return response()->json([
            'success' => true,
            'data' => $order
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $order = Order::find($id);
        $data = $request->all();

        if (isset($data['price'])) {
            $orderData = [];
            $orderData['user_id'] = Auth::id();
            $orderData['price'] = $data['price'];
            $order->update($orderData);
            $paymentData = [];
            $paymentData['price'] = $data['price'];
            $order->payment()->update($paymentData);
        }


        if (isset($data['products'])) {
            $order->products()->sync($data['products']);
        }

        if (isset($data['paymenttype'])) {
            $paymentData = [];
            $paymentData['type'] = $data['paymenttype'];
             $order->payment()->update($paymentData);
        }

        return response()->json([
            'success' => true,
            'data' => $order
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
