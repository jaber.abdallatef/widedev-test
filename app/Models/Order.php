<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Product;
use App\Models\Payment;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'price',
         'user_id',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }
}
