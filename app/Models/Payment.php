<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use app\models\Order;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'price',
        'type',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
