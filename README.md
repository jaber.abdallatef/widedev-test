<p align="center"><a href="https://widedev.io/" target="_blank"><img src="https://widedev.io/wp-content/uploads/2021/09/WideDev.png" width="400"></a></p>



## About Wide Dev - Test

Restfull API for small ecommerce system using the following techs 

- Laravel 8 
- Php 7.4 or 8 
- MySql 5.7.24

## Run The Project using the following steps :
1- create mysql empty db 
2- run php artisan migrate 
3- run php artisan db:seed 

## View Rest APi docs 
go  to http://localhost:8000/request-docs


## Test The Project 
 auth/register
 body 
 {
     "email" : "jaber12@hotmail.com",
     "password" : "jaber@90",
     "name" : "jaber",
     "surname" : "dx"

}

auth/login 
 {
     "email" : "jaber12@hotmail.com",
     "password" : "jaber@90",
}

now use access token in the header as the following 
  headers: { 
    'Authorization': 'Bearer  {{access token}}
  }

POST
orders
{
     "products" : ["5" , "6" , "2"],
     "paymenttype" : "creditcard", "price" : 100
}

PUT 
orders/{order_id}  : for updating order
{
     "products" : ["5" , "6" , "2"], // optional
     "paymenttype" : "creditcard",   // optional
      "price" : 100                  // optional
}

GET  
orders/{order_id} : for order details 

GET  
orders?userid={user_id} : for listing orders  by user 
